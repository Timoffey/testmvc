<?php
// Тут управление событиями
include_once("m/list.php");
include_once("m/task_add.php");
if($_POST['ap_task'])					// Если была нажата кнопка подтверждения о выполнении задачи
	task_approve($_POST['ap_task']);

if($_POST['ed_task']){					// Если была нажата кнопка редактирования текста. Топорное решение, зато лёгковесное.
	$data=task_edit($_POST['ed_task']);	// Мне стало жалко времени, мой перфекционизм отожрал его слишком много
	echo'<form method="post">
		<textarea name="text" rows="10" cols="45">'.$data['text'].'</textarea>
		<input type="hidden" name="id" value='.$data['id'].'>
		<input type="submit" value = "update">
	</form>';
}

if ($_POST['text'])						// Перехватываем новый текст и отправляем в БД
	task_update($_POST['text'],$_POST['id']);

if ($_GET['page']){						// Переключаем странички
	$page=$_GET['page'];
}
if ($_GET['order']){					// И сортировка
	$order=$_GET['order'];
	$_SESSION['order']=$order;
}
if ($_SESSION['order']) {				// Чтобы можно было вместе с пагинатором
	$order=$_SESSION['order'];}

$list=get_tasks($page,$order);