<?php
// Загрузка и обработка картинки
session_start();
require_once('../m/SimpleImage.php');           // Подключаем библиотеку для ресайза
require_once('../m/upload.php');                // Загрузка изображения по стандартам W3
$rez=checkAndUpload();                          // Получаем разрешение загруженного файла
$x=$rez['x'];
$y=$rez['y'];
$target_file=$_SESSION['file'];
$image = new SimpleImage();
$image->load($target_file);
if ($x-$y > 0)                                  // И изменяем разрешение по большей стороне
    $image->resizeToWidth(320);
else 
    $image->resizeToHeight(240);
$image->save($target_file);
header('Location: ../v/add.php');
?>