<?php
// Этот файл получился довольно объёмным. Из-за вёрстки.
// Я много времени убил на асинхронную передачу данных, но потом плюнул.
// В итоге сначала загружается картинка, потом дописываются остальные данные
session_start();
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Задачник</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container mt-3">
	<?php
// В случае, если файл уже загружен, либо не прошёл проверку, уведомляем об этом пользоваетеля (с демонстрацией каринки)
	if ($_SESSION['file']){
		echo '
		<div class="jumbotron">
			<div class="row">
				<div class="col-sm-8"><h3>';
					echo ($_SESSION['result']);
					echo '</h3>
				</div>
				<div class="col-sm-4">
					<img src='.$_SESSION['file'].'>
				</div>
			</div>
		</div>';
	} else {			// Иначе предоставляем контрол для загрузки. Он уже на старте фильтрует расширения файла
		echo'
	<form action="../c/upload.php" method="post" enctype="multipart/form-data">
		<div class="row">
			<div class="col-sm-7">
				<div class="custom-file">
				    <input type="file" accept=".gif,.jpeg,.jpg,.png"class="custom-file-input" id="img" name="img" onchange="check()">
				    <label class="custom-file-label" for="img">Choose picture (PNG, JPG, GIF) 320x240 px</label>
				</div>
			</div>
			<div class="col-sm-2">
				<button type="submit" id="upload" class="btn btn-primary" disabled>Upload</button>
			</div>
		</div>
	</form>';
	}
	?>
	<form action = "../c/add.php" method="post">
		<div class="row mt-5">
			<div class="col">
				<div class="form-group">
					<label for="mail">Email address:</label>
					<input type="mail" class="form-control" id="mail" name="mail">
				</div>
			</div>
			<div class="col">
				<div class="form-group">
					<label for="name">Name:</label>
					<input type="text" class="form-control" id="name" name="name">
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col">
				<div class="form-group">
	  				<label for="task">Task:</label>
	  				<textarea class="form-control" rows="5" id="task" name="task"></textarea>
				</div>
			</div>
		</div>
		<div class="row">
			
			<div class="col-sm-10">
				<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal" onclick="preview()">Preview</button>
			</div>
			<div class="col-sm-1">
				<button type="submit" class="btn btn-primary">Save</button>
			</div>
		</div>
	</form>
</div>
<!-- Далее модальное окно для предпросмотра будущей карточки -->
<!-- The Modal -->
<div class="modal" id="myModal">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">

			<!-- Modal Header -->
			<div class="modal-header">
				<h4 class="modal-title">Task preview</h4>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>

			<!-- Modal body -->
			<div class="modal-body">
				<div class="media border p-3">
  					<img src="<?=$_SESSION['file']?>" class="mr-3 mt-3 rounded">
 					<div class="media-body">
    					<div class="row">
      						<div class="col">
        						<h4 id="namePreview"><?=$name?></h4>
      						</div>
      						<div class="col">
        						<h4><small><i id="mailPreview"><?=$mail?></i></small></h4>
      						</div>
    					</div>
    					<p id = "taskPreview"><?=$text?></p>      
  					</div>
				</div>
			</div>

			<!-- Modal footer -->
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
			</div>

		</div>
	</div>
</div>
<script>
function check(){											// Функция, активирующая кнопку загрузки файла, после выбора оного
	if(document.getElementById("img").value != "") {
		document.getElementById("upload").disabled = false;
	}
}															// Заменяем шаблон в модальном окне на актуальные значения
function preview(){
	document.getElementById('namePreview').innerHTML=document.getElementById('name').value;
	document.getElementById('mailPreview').innerHTML=document.getElementById('mail').value;
	document.getElementById('taskPreview').innerHTML=document.getElementById('task').value;

}
</script> 
<?php

?>
</body>
</html>