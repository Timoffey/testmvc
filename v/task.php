<!--Карточка задания-->
<!--В первой строчке определяется цвет карточки – выполнена она или нет-->
<div class="media border p-3 <?=$status ? 'table' : 'table-danger'?>">
  <!--Картинка, соответствующая id записи в БД-->
  <img src="img/<?=$id?>.<?=$type?>" class="mr-3 mt-3 rounded">
  <div class="media-body">
    <div class="row">
      <div class="col-sm-6">
        <h4><?=$name?></h4>
      </div>
      <div class="col-sm-5">
        <h4><small><i><?=$mail?></i></small></h4>
      </div>
      <div class="col-sm-1">
        <?php
          if($_SESSION['role']=='admin'){                           // Если залогинен под админом появляется два контрола
            echo'<form method="post">
              <h5><small><i>
                <input type="submit" value="Approve">               <!--На подтверждение о выполнении-->
                <input type="hidden" name="ap_task" value='.$id.'>
              </i></small></h5>
            </form>';
            echo'<form method="post">
              <h5><small><i>
                <input type="submit" value="Edit">                  <!--На редактирование текста-->
                <input type="hidden" name="ed_task" value='.$id.'>
              </i></small></h5>
            </form>';
          }?>
      </div>
    </div>
    <p><?=$text?></p>      
  </div>
</div>