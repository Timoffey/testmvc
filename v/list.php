<?php
// Принимаем массив элементов списка (не более трёх) с массивами значений от модели
include_once("m/list.php");
$pages=get_count();							// Узнаём сколько всего задач, для выведения количества страниц
include_once("c/page_control.php");			// Тут будем ловить текущую страницу, и основные контролы

foreach ($list as $element){				// Собираем данные задачи от модели в переменные
	$id=$element['id'];
	$status=$element['status'];
	$name=$element['name'];
	$mail=$element['mail'];
	$text=$element['text'];
	$type=$element['img_type'];
	include("v/task.php");					// И передаём их во вьюху
}
// Дописываем пагинацию
?>
<ul class="pagination justify-content-center">
<?php
	for ($i = 1; $i<=$pages; $i++){
	echo "<li class='page-item'><a class='page-link' href='?page=$i'>$i</a></li>";		
	}
?>
</ul>