<?php
// Та самая функция по стандартам W3 ^_^
function checkAndUpload(){
	$target_dir = "../img/";
	$target_file = $target_dir . basename($_FILES["img"]["name"]);
	$uploadOk = 1;
	$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));			// Получаем расширение
	// Check if image file is a actual image or fake image
	$check = getimagesize($_FILES["img"]["tmp_name"]);								

	if($check !== false) {															
	    $result = "File is an image - " . $check["mime"] . ".";
	    $uploadOk = 1;
	} else {
	    $result = "File is not an image.";
	    $uploadOk = 0;
	}
	// Check if file already exists
	if (file_exists($target_file)) {
	    $result = "Sorry, file already exists.";
	    $uploadOk = 0;
	}
	// Check file size
	if ($_FILES["img"]["size"] > 10000000) {
	    $result = "Sorry, your file is too large.";
	    $uploadOk = 0;
	}
	// Allow certain file formats
	if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
	&& $imageFileType != "gif" ) {
	    $result = "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
	    $uploadOk = 0;
	}
	// Check if $uploadOk is set to 0 by an error
	if ($uploadOk == 0) {
	    $result = "Sorry, your file was not uploaded.";
	// if everything is ok, try to upload file
	} else {
	    $target_file = $target_dir . basename($_FILES["img"]["tmp_name"]).'.'.$imageFileType;
	    $_SESSION['fileType']=$imageFileType;
	    if (move_uploaded_file($_FILES["img"]["tmp_name"], $target_file)) {
	        $_SESSION['file']=$target_file;
	        $result = " The file has been successfully uploaded.";
	    } else {
	        $result = "Sorry, there was an error uploading your file.";
	    }
	}
	$_SESSION['result']=$result;
	$x=$check[0];       // Ожидаем 320
	$y=$check[1];       // Ожидаем 240
	return Array('x'=>$x,'y'=>$y);
}