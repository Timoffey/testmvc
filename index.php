<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Задачник</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
  <!--Последующие строки чтобы выпадающий список сортировки подходил по цвету-->
  <style>
  	.navbar-nav > li > .dropdown-menu { background-color: #343a40; }
	.dropdown-item { color: white; }
  </style>
  <!--------------------------------------------------------------------------->
</head>
<body>
<nav class="navbar navbar-expand-sm bg-dark navbar-dark">
  <!-- Brand -->
  <a class="navbar-brand" href="#">Задачник</a>
  <form class="form-inline" action="/v/add.php">
  	<button class="btn btn-success" type="submit">Add task</button>
  </form>
  <!-- Links -->
  <ul class="navbar-nav mx-auto">

    <!-- Dropdown -->
    <li class="nav-item dropdown">
      <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
        Sort tasks by
      </a>
      <div class="dropdown-menu">
        <a class="dropdown-item" href="?order=name">Name</a>
        <a class="dropdown-item" href="?order=mail">E-mail</a>
        <a class="dropdown-item" href="?order=status">Status</a>
      </div>
    </li>
    <!--Бэдж, отображающий текущую сортировку-->
    <span class="badge m-2 badge-secondary"><?session_start();echo($_SESSION['order'])?></span>
  </ul>
  <?php
  if ($_SESSION['role']=='admin') echo '<div class="text-danger">Admin</div>';			// Возможность залогиниться администратору
  else echo '<a class="btn btn-dark" href="v/login.php">Log in</a>';					// Разлогинивание не предусмотрено
  ?>
</nav>
	<?php
	include_once("v/list.php");
	?>
</body>
</html>